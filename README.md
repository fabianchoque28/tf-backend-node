## Trabajo Final Backend con NODE
#### Actualizado Julio 2023

El siguiente proyecto se realizó como propuesta al trabajo final correspondiente al modulo 04 - PILIS para la reserva de tickets usando [node.js](https://nodejs.org/es)

## Ejecucion

### 1 Configurar la variable de entorno 

Renombrar el archivo __.env.template__ a __.env__ y setear las variables indicadas

### 2 Crear el contenedor de Docker

```bash
    docker compose up
```
### 3 Instalar Dependencias

```bash
    npm i
```

### 4 Correr servidor local

```bash
    npm run dev
```

### 5 Rutas de acceso
[http://localhost:3000/api/](http://localhost:3000/api)
Para consulta a la api

[http://localhost:3000/docs](http://localhost:3000/docs)
Para acceso a la interfaz de Swagger
