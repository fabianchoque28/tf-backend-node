import { Request, Response, NextFunction } from 'express';
import jwt, { JwtPayload }  from 'jsonwebtoken';
import dotenv from 'dotenv';
import passport from 'passport'

import { User } from '../entity/User';

dotenv.config();

export const authRequired = async (req: Request, res: Response, next: NextFunction) => {
    const authorizationHeader  = req.headers.authorization
    if (!authorizationHeader) {
        return res.status(401).json({ msg: "No token, authoration denied" });
    }
    const token = authorizationHeader.split(' ')[1];
    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY!) as JwtPayload;
        req.user = decoded
        next();
    } catch (error) {
        return res.status(401).json({ msg: "Invalid Token" });
    }
};

export const roleAdminRequired = async (req: Request, res: Response, next: NextFunction) => {
    
    if (req.user) {
        const {id} = req.user as JwtPayload;
        try {
            const user = await User.findOneBy({ id });
            if (user?.role == "admin") {
                next();
                return;
            }
            return res.status(403).json({ msg: "Invalid Role" });    
        } catch (error) {
            console.log(error);
        }
    }
};
