import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import passport from 'passport'

import swaggerUi from "swagger-ui-express";
import swaggerJsDoc from 'swagger-jsdoc';
import {options} from './swaggerOptions'

import userRoutes from './routes/user.router';
import authRoutes from './routes/auth.router';
import eventRoutes from './routes/event.router';
import bookingRoutes from './routes/booking.router';
import passportMiddleware from './middlewares/passport';

const app = express()
app.use(morgan('dev'));
app.use(cors());
app.use(express.json());

app.use(express.urlencoded({extended: false}));
app.use(passport.initialize());
passport.use(passportMiddleware);

const specs = swaggerJsDoc(options);
app.use(
    "/docs",
    swaggerUi.serve,
    swaggerUi.setup(specs)
);

app.use("/api/users", userRoutes);
app.use("/api/auth", authRoutes);
app.use("/api/events", eventRoutes);
app.use("/api/bookings", bookingRoutes);

export default app;

