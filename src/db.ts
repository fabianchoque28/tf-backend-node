import { DataSource } from 'typeorm';
import dotenv from 'dotenv';
import { User } from './entity/User';
import { Event } from './entity/Event';
import { Booking } from './entity/Booking';

dotenv.config();

export const AppDataSource = new DataSource({
   type: 'mysql',
   host: process.env.DB_HOST,
   port: process.env.DB_PORT ? parseInt(process.env.DB_PORT, 10) : undefined,
   username: process.env.DB_USER,
   password: process.env.DB_PASSWORD,
   database: process.env.DB_DATABASE,
   synchronize: true,
   entities: [User, Event, Booking] 
})