import { Router } from 'express';

import {
    getUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser
} from '../controllers/user.controller';
import { authRequired, roleAdminRequired } from '../middlewares/permission';

const router = Router();

router.get('/', [authRequired, roleAdminRequired], getUsers);
router.get('/:id', [authRequired, roleAdminRequired], getUser);
router.post('/',[authRequired, roleAdminRequired], createUser);
router.put('/:id', [authRequired, roleAdminRequired], updateUser);
router.delete('/:id', [authRequired, roleAdminRequired], deleteUser);

export default router;