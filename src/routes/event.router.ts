import { Router } from 'express';

import {
    getEvents,
    getEvent,
    createEvent,
    updateEvent,
    deleteEvent
} from '../controllers/event.controller';
import { authRequired, roleAdminRequired } from '../middlewares/permission';

const router = Router();

router.get('/', getEvents);
router.get('/:id', getEvent);
router.post('/', [authRequired, roleAdminRequired], createEvent);
router.put('/:id', [authRequired, roleAdminRequired], updateEvent);
router.delete('/:id', [authRequired, roleAdminRequired], deleteEvent);

export default router;