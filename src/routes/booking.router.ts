import { Router } from 'express';

import {
    getBookings,
    getBooking,
    createBooking,
    updateBooking,
    deleteBooking
} from '../controllers/booking.controller';
import { authRequired, roleAdminRequired } from '../middlewares/permission';

const router = Router();

router.get('/', getBookings);
router.get('/:id', getBooking);
router.post('/', [authRequired, roleAdminRequired], createBooking);
router.put('/:id', [authRequired, roleAdminRequired], updateBooking);
router.delete('/:id', [authRequired, roleAdminRequired], deleteBooking);

export default router;