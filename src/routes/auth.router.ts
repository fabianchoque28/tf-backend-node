import { Router } from 'express';
import passport from 'passport'

import {signIn, protectedEndpoint, refresh } from '../controllers/auth.controller'

const router = Router();

router.post('/signin', signIn);
router.post('/token', refresh);
router.post('/protected', passport.authenticate('jwt', { session: false }), protectedEndpoint);

export default router;