import {
    Entity,
    Column, 
    PrimaryGeneratedColumn,
    BaseEntity,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { User } from "./User";
import { Event } from "./Event";

@Entity() 
export class Booking extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    dateEvent: Date;

    @Column()
    location: string;

    @Column()
    gps: string;

    @Column({ type: 'float' })
    price: number;

    @ManyToOne(() => User, (user) => user.bookings)
    @JoinColumn({ name: 'userId' })
    user: User;
  
    @ManyToOne(() => Event, (event) => event.bookings)
    @JoinColumn({ name: 'eventId' })
    event: Event;
}
