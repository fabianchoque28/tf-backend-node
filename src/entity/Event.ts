import {
    Entity,
    Column, 
    PrimaryGeneratedColumn,
    BaseEntity,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
} from "typeorm";
import { Booking } from "./Booking";

@Entity() 
export class Event extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    type: string;

    @Column()
    dateEvent: Date;

    @Column()
    location: string;

    @Column()
    img_url: string;

    @Column()
    gps: string;

    @Column({ type: 'float' })
    price: number;

    @Column()
    limit: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(() => Booking, (booking) => booking.user)
    bookings: Booking[];
}
