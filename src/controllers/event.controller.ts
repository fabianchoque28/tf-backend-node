import { Request, Response } from "express";
import moment from 'moment-timezone';

import { Event } from '../entity/Event';

const TIME_ZONE = 'America/Argentina';
const FORMAT_DATE = 'DD/MM/YYYY-HH:mm';

export const getEvents = async (req: Request, res: Response) => {
    try {
        const events = await Event.find();
        
        return res.json(events);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }       
};

export const getEvent = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const event = await Event.findOne({
            where: { id: parseInt(id)}
        })
        if (!event) return res.status(404).json({ message: "Event not found" });
        return res.json(event);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }      
};
    
export const updateEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const event = await Event.findOneBy({id: parseInt(id)});
        if (!event) return res.status(404).json({message: "Event not found"});

        if (req.body.dateEvent) {
            const date = validateDate(req.body.dateEvent);
            if (!date) {
                return res.status(500).json({msg: "Incorrect date format, dd/mm/yyyy-hh:mm"});
            }
            req.body.dateEvent = date
        }
        
        await Event.update({id: parseInt(id)}, req.body);
        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({message: error.message});
        }
    }
};

export const deleteEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const result = await Event.delete({id: parseInt(id)});
        if (result.affected === 0) {
            return res.status(404).json({message: "Event not found"})
        }

        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({message: error.message});
        }
    }
};

export const createEvent = async (req: Request, res: Response ): Promise<Response> => {

    if (!req.body.name || !req.body.type || !req.body.dateEvent || 
        !req.body.location || !req.body.gps || !req.body.img_url || !req.body.price || !req.body.limit) {
        return res
            .status(400)
            .json({ msg: "Please. Send all data: name, type, dateEvent, location, img_url, gps, price, limit" });
    }
    const date = validateDate(req.body.dateEvent);

    if (!date) {
        return res.status(500).json({msg: "Incorrect date format, dd/mm/yyyy-hh:mm"});
    }

    const event = await Event.findOneBy({ name: req.body.name });
    if (event) {
        return res.status(400).json({ msg: "The Event already Exists" });
    }

    const newEvent = new Event();
    newEvent.name = req.body.name;
    newEvent.type = req.body.type;
    newEvent.location = req.body.location;
    newEvent.dateEvent = date;
    newEvent.gps = req.body.gps;
    newEvent.img_url = req.body.img_url;
    newEvent.price = req.body.price;
    newEvent.limit = req.body.limit;


    await newEvent.save();
    return res.status(201).json(newEvent);
};

export const validateDate = (dateTimeString : string )=> {
    const regex = /^(\d{2})\/(\d{2})\/(\d{4})-(\d{2}):(\d{2})$/;
    if (dateTimeString.match(regex)) {
        const dateMoment = moment.tz(dateTimeString, FORMAT_DATE, TIME_ZONE);
        return dateMoment.toDate();
    }
}