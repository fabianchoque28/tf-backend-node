import { Request, Response } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import dotenv from 'dotenv';

import { User } from "../entity/User";

dotenv.config();

let refreshTokens: (string | undefined)[] = [];

export const signIn = async (req: Request, res: Response): Promise<Response> => {
    if (!req.body.username || !req.body.password) {
        return res
            .status(400)
            .json({ msg: "Please. Send your username and password" });
    }

    const user = await User.findOneBy({ username: req.body.username });

    if (!user) {
        return res.status(400).json({ msg: "The User does not exists" });
    }

    const isMatch = await comparePassword(user, req.body.password);

    if (isMatch) {
        return res.status(400).json({ credentials: createToken(user) });
    }

    return res.status(400).json({
        msg: "The username or password are incorrect"
    });
};

export const refresh = async (req: Request, res: Response): Promise<any>  => {
    const refreshToken = req.body.refresh;
    // If token is not provided, send error message
    if (!refreshToken) {
        res.status(401).json({
        errors: [{msg: "Token not found",},],
        });
    }
    // If token does not exist, send error message
    if (!refreshTokens.includes(refreshToken)) {
        res.status(403).json({
        errors: [{ msg: "Invalid refresh token",},],
        });
    }

    try {
        const user = jwt.verify(refreshToken, process.env.JWT_SECRET_KEY_REFRESH!);
        // user = { email: 'jame@gmail.com', iat: 1633586290, exp: 1633586350 }
        const { username } = <any>user;
        const userFound = <User> await User.findOneBy({ username: username });
        if (!userFound) {
            return res.status(400).json({ msg: "The User does not exists" });
        }
        const accessToken = jwt.sign({ id: userFound.id, email: userFound.username }, process.env.JWT_SECRET_KEY!, {expiresIn: '30s'});
        res.json({ accessToken });
    } catch (error) {
        res.status(403).json({
        errors: [{msg: "Invalid token",},],
        });
    }
};

export const createToken = (user: User) => {
    const token = jwt.sign({ id: user.id, username: user.username }, process.env.JWT_SECRET_KEY!, {expiresIn: '1d'});
    const refreshToken = jwt.sign({ username: user.username }, process.env.JWT_SECRET_KEY_REFRESH!, {expiresIn: '90d'});
    refreshTokens.push(refreshToken);
    return {
        token,
        refreshToken
    }
}

export const protectedEndpoint = async (req: Request, res: Response): Promise<Response> => {
    return res.status(200).json({ msg: 'ok'});
}

export const comparePassword = async (user: User, password: string ): Promise<Boolean> => {
    return await bcrypt.compare(password, user.password);
};

export const createHash = async (password: string ): Promise<string> => {
    const saltRounds = 10;
    return await bcrypt.hash(password, saltRounds);
};
