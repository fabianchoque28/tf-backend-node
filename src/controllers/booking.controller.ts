import { Request, Response } from "express";

import { Booking } from '../entity/Booking';
import { Event } from '../entity/Event';
import { User } from '../entity/User';
import { validateDate } from "./event.controller";

const SELECT_FIELDS = {
    select: {
        id: true,
        user: {
            id: true,
            fullname: true,
        },
        event: {
            id: true,
            name: true
        },
        dateEvent: true,
        location: true,
        gps: true,
        price: true
      },
    relations: {
        user: true,
        event: true,
    }
}

export const getBookings = async (req: Request, res: Response) => {
    try {
        const bookings = await Booking.find(
            SELECT_FIELDS 
        );
        
        return res.status(200).json(bookings);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }       
};

export const getBooking = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const booking = await Booking.findOne({
            where: { id: parseInt(id)},
            ...SELECT_FIELDS
        })
        if (!booking) return res.status(404).json({ message: "Booking not found" });
        return res.json(booking);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }      
};
    
export const updateBooking = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const booking = await Booking.findOne({
            where: { id: parseInt(id)}
        })
        if (!booking) return res.status(404).json({message: "Booking not found"});
        
        if (req.body.dateEvent) {
            const date = validateDate(req.body.dateEvent);
            if (!date) {
                return res.status(500).json({msg: "Incorrect date format, dd/mm/yyyy-hh:mm"});
            }
            req.body.dateEvent = date
        }

        await Booking.update({id: parseInt(id)}, req.body);
        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({message: error.message});
        }
    }
};

export const deleteBooking = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const result = await Booking.delete({id: parseInt(id)});
        if (result.affected === 0) {
            return res.status(404).json({message: "Booking not found"})
        }

        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({message: error.message});
        }
    }
};

export const createBooking = async (req: Request, res: Response ): Promise<Response> => {

    if (isNaN(req.body.user) || isNaN(req.body.event)) {
        return res
            .status(400)
            .json({ msg: "Please. Send user and event" });
    }

    const user = await User.findOne({ where: { id: req.body.user}})
    if (!user) return res.status(404).json({message: "User not found"});

    const event = await Event.findOne({ where: { id: req.body.event}})
    if (!event) return res.status(404).json({message: "Event not found"});

    const bookingExisting = await Booking.find({
        relations: {
            user: true,
            event: true,
        },
        where: {
            user: {
                id: user.id
            },
            event: {
                id: event.id
            },
        },
    })
    
    if (bookingExisting.length > 0) return res.status(400).json({ msg: "The User already made a Booking in that Event" });

    const usersInEvent = await Booking.find({
        relations: {
            event: true,
        },
        where: {
            event: {
                id: event.id
            },
        },
    })

    if (usersInEvent.length == event.limit) {
        return res.status(404).json({message: "Event full"});
    }

    const newBooking = new Booking();

    newBooking.event = event
    newBooking.user = user
    newBooking.dateEvent = event.dateEvent;
    newBooking.location = event.location;
    newBooking.gps = event.gps;
    newBooking.price = event.price;

    await newBooking.save();
    return res.status(201).json(newBooking);

};
