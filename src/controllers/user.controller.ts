import { Request, Response } from "express";

import { User } from "../entity/User";
import { createHash } from "./auth.controller";

export const getUsers = async (req: Request, res: Response) => {
    try {
        // const users = await User.find();
        const users = await User.find({
            select: {
                id: true,
                fullname: true,
                username: true,
                role: true
            },
        });
        
        return res.json(users);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }       
};

export const getUser = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const user = await User.findOne({
            where: { id: parseInt(id)},
            select: {
                id: true,
                fullname: true,
                username: true,
                role: true
            },
        })
        if (!user) return res.status(404).json({ message: "User not found" });
        return res.json(user);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }      
};
    
export const updateUser = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const user = await User.findOneBy({id: parseInt(id)});
        if (!user) return res.status(404).json({message: "User not found"});

        await User.update({id: parseInt(id)}, req.body);
        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({message: error.message});
        }
    }
};

export const deleteUser = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const result = await User.delete({id: parseInt(id)});
        if (result.affected === 0) {
            return res.status(404).json({message: "User not found"})
        }

        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({message: error.message});
        }
    }
};

export const createUser = async (req: Request, res: Response ): Promise<Response> => {
    if (!req.body.fullname || !req.body.username || !req.body.password) {
        return res
            .status(400)
            .json({ msg: "Please. Send your name, username and password" });
    }

    const user = await User.findOneBy({ username: req.body.username });
    if (user) {
        return res.status(400).json({ msg: "The User already Exists" });
    }

    const newUser = new User();
    newUser.fullname = req.body.fullname;
    newUser.username = req.body.username;
    newUser.password = await createHash(req.body.password);
    await newUser.save();
    return res.status(201).json(newUser);
};
    